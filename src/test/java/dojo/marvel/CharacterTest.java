package dojo.marvel;

import br.com.treinamento.config.AppConfig;
import br.com.treinamento.dojo.exception.ServiceException;
import br.com.treinamento.dojo.marvel.model.Character;
import br.com.treinamento.dojo.service.CharacterService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertNotNull;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=AppConfig.class, loader=SpringApplicationContextLoader.class)
public class CharacterTest {
	
	@Autowired
	private CharacterService characterService;
	
	@Test
	public void getByNameCharacter(){
		Character detail = null;
		try {
			detail = characterService.getCharacterByName("Luke Cage");
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		assertNotNull(detail);
	}
}
