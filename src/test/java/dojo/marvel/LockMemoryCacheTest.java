package dojo.marvel;

import br.com.treinamento.config.AppConfig;
import br.com.treinamento.dojo.cache.LockMemoryCache;
import br.com.treinamento.dojo.marvel.model.Character;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=AppConfig.class, loader=SpringApplicationContextLoader.class)
public class LockMemoryCacheTest {

	@Autowired
	private LockMemoryCache lockMemoryCache;
	
	@Test
	public void testAddingAndGettingValue() {
		Character detail = new Character();
		detail.setCharacterId(1);
		detail.setName("Best Programmers");
		detail.setTitle("The hard work produce success.");

		LockMemoryCache.getInstance().add(detail);
				
		assertNotNull(LockMemoryCache.getInstance().get(1));

        Character newMarvelDetail = LockMemoryCache.getInstance().get(1);

        assertNotNull(newMarvelDetail);
		assertTrue(detail.getName().equals(newMarvelDetail.getName()));
	}

}
