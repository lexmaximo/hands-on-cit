package br.com.treinamento.dojo.marvel.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Estrutura modelo para informações de comic.
 * Created by Alex Ricardo on 10/11/2016.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Comic  {

    private Integer digitalId;

    private String title;

    private double issueNumber;

    private String variantDescription;

    private String isbn;

    private String upc;

    private String diamondCode;

    private String ean;

    /**
     * GETS/SETS
     */

    public int getDigitalId() {
		return digitalId;
	}

	public void setDigitalId(int digitalId) {
		this.digitalId = digitalId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getIssueNumber() {
		return issueNumber;
	}

	public void setIssueNumber(double issueNumber) {
		this.issueNumber = issueNumber;
	}

	public String getVariantDescription() {
		return variantDescription;
	}

	public void setVariantDescription(String variantDescription) {
		this.variantDescription = variantDescription;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getUpc() {
		return upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

	public String getDiamondCode() {
		return diamondCode;
	}

	public void setDiamondCode(String diamondCode) {
		this.diamondCode = diamondCode;
	}

	public String getEan() {
		return ean;
	}

	public void setEan(String ean) {
		this.ean = ean;
	}

	@Override
	public String toString() {
		return "Comic [digitalId=" + digitalId + ", title=" + title
				+ ", issueNumber=" + issueNumber + ", variantDescription="
				+ variantDescription + ", isbn=" + isbn + ", upc=" + upc
				+ ", diamondCode=" + diamondCode + ", ean=" + ean +  "]";
	}
}
