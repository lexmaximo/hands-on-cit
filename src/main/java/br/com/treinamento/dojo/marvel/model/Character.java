package br.com.treinamento.dojo.marvel.model;

import java.util.List;

/**
 * Esturura para tratar as informações de comic.
 * Created by Alex Ricardo on 10/11/2016.
 */
public class Character {

    private Integer characterId;

    private String name;

    private String title;

    private List<Comic> comics;

    public Integer getCharacterId() {
        return characterId;
    }

    public void setCharacterId(Integer characterId) {
        this.characterId = characterId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Comic> getComics() {
        return comics;
    }

    public void setComics(List<Comic> comics) {
        this.comics = comics;
    }

    @Override
    public String toString() {
        return "ComicDetail [characterId=" + characterId + ", name=" + name
                + ", title=" + title + "]";
    }
}
