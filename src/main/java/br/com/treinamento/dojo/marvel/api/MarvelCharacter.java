package br.com.treinamento.dojo.marvel.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Etrutura simples de retorno embutida na ApiReturn.
 * Created by Alex Ricardo on 10/11/2016.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarvelCharacter  {
	
	private Integer id;

    private String name;
    
    private String description;

	/**
	 * GETS/SETS
	 */

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "MarvelCharacter [id=" + id + ", name=" + name
				+ ", description=" + description + ", thumbnail="
				+ "]";
	}
}