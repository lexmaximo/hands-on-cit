package br.com.treinamento.dojo.exception;

/**
 * Created by Alex Ricardo on 10/11/2016.
 */
public class ServiceException extends Exception{

    public ServiceException(String message) {
        super(message);
    }
}
