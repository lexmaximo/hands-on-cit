package br.com.treinamento.dojo.controller;

import br.com.treinamento.dojo.exception.ServiceException;
import br.com.treinamento.dojo.marvel.model.Comic;
import br.com.treinamento.dojo.marvel.model.Character;
import br.com.treinamento.dojo.service.CharacterService;
import com.google.gson.Gson;
import jersey.repackaged.com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Rest Controller de conulsta de personagens
 * Created by Alex Ricardo on 10/11/2016.
 */
@Controller
@RestController
public class CharacterController {

    @Autowired
    private CharacterService characterService;

    /**
     * Consulta o personagem pelo nome
     * @param characterName
     * @return
     */
    @RequestMapping(value = "/character", method = RequestMethod.GET)
    public ResponseEntity<String> characterDetail(@RequestParam String characterName) {

        Character result = null;
        try {
            result = characterService.getCharacterByName(characterName);
        } catch (ServiceException e) {
            e.printStackTrace();
            return new ResponseEntity<String>(
                    "Ocorreu um erro na consulta, informações não disponíveis.",
                    HttpStatus.BAD_REQUEST);
        }
        String json = new Gson()
                .toJson(result);

        return new ResponseEntity<String>(json, HttpStatus.OK);
    }

    /**
     * Consulta a lista de comics pelo id do personagem.
     * @param characterId
     * @return
     */
    @RequestMapping(value = "/comics", method = RequestMethod.GET)
    public ResponseEntity<String> getComicsByCharacter(@RequestParam Integer characterId) {

        List<Comic> result = Lists.newArrayList();
        try {
            result = characterService.getComicByID(characterId).getData().getResults();
        } catch (ServiceException e) {
            e.printStackTrace();
            return new ResponseEntity<String>(
                    "Ocorreu um erro na consulta, informações não disponíveis.",
                    HttpStatus.BAD_REQUEST);
        }

        String json = new Gson()
                .toJson(result);

        return new ResponseEntity<String>(json, HttpStatus.OK);
    }
}
