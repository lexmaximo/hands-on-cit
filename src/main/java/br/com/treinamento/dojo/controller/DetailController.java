package br.com.treinamento.dojo.controller;

import br.com.treinamento.dojo.marvel.model.Character;
import br.com.treinamento.dojo.service.DetailService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * Rest controller de manutenção de details e informações em memória.
 * Created by Alex Ricardo on 10/11/2016.
 */
@Controller
@RestController
public class DetailController {

    @Autowired
    private DetailService detailService;

    /**
     * Consulta os detalhes de um comic em cache.
     * @param id
     * @return
     */
    @RequestMapping(value = "/comicDetail/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE )
    public ResponseEntity<String> getComic(@PathVariable Integer id) {
        Character result = detailService.getCharacter(id);

        if(Objects.isNull(result)){
            throw new RuntimeException("This ID not found!");
        }

        String json = new Gson()
                .toJson(result);
        return new ResponseEntity<String>(json, HttpStatus.OK);
    }

    /**
     * Atualiza os detalhes de um comic em cache.
     * @param comicDetail
     * @return
     */
    @RequestMapping(value = "/comicDetail", method = RequestMethod.PUT)
    public ResponseEntity<Character> updateComic(@RequestBody Character comicDetail) {
        detailService.insertOrUpdate(comicDetail);

        return new ResponseEntity<Character>(comicDetail, HttpStatus.OK);
    }

    /**
     * Insere um novo comic em cache.
     * @param comicDetail
     * @return
     */
    @RequestMapping(value = "/comicDetail", method = RequestMethod.POST)
    public ResponseEntity<Character> insertComic(@RequestBody Character comicDetail) {
        detailService.insertOrUpdate(comicDetail);

        return new ResponseEntity<Character>(comicDetail, HttpStatus.OK);
    }
}
