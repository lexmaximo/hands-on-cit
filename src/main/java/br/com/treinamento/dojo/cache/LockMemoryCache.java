package br.com.treinamento.dojo.cache;

import br.com.treinamento.dojo.marvel.model.Character;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Manager de cache para consultas já realizadas
 * Created by Alex Ricardo on 10/11/2016.
 */
@Component
public class LockMemoryCache {

    private final static LockMemoryCache instance_cache = new LockMemoryCache();
    public Map<Integer, Character> cache = new HashMap<>();

    /**
     * Retorna uma instância da class que gerencia as informações em memória.
     * @return
     */
    public static LockMemoryCache getInstance() {
        return instance_cache;
    }

    /**
     * Insere um novo registro na lista em memória
     * @param comicDetail
     */
    public void add(Character comicDetail) {
        cache.put(comicDetail.getCharacterId(), comicDetail);

    }

    /**
     * Retorna o registro da lista em memória com o id informado.
     * @param characterId
     * @return
     */
    public Character get(Integer characterId) {
        return cache.get(characterId);
    }

    /**
     * Remove o detail consultado da lista em memória pelo id.
     * @param characterId
     * @return
     */
    public Character remove(Integer characterId) {
        return cache.remove(characterId);
    }
}
