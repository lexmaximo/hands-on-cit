package br.com.treinamento.dojo.service;

import br.com.treinamento.dojo.cache.LockMemoryCache;
import br.com.treinamento.dojo.marvel.model.Character;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Trata a memória dos registros.
 * Created by Alex Ricardo on 10/11/2016.
 */
@Component
public class DetailService {

    @Autowired
    private LockMemoryCache lockMemoryCache;

    /**
     *
     * @param id
     * @return
     */
    public Character getCharacter(Integer id) {
        return LockMemoryCache.getInstance().get(id);
    }

    /**
     *
     * @param comicDetail
     */
    public void insertOrUpdate(Character comicDetail) {
        LockMemoryCache.getInstance().add(comicDetail);
    }
}
