package br.com.treinamento.dojo.service;

import br.com.treinamento.dojo.cache.LockMemoryCache;
import br.com.treinamento.dojo.exception.ServiceException;
import br.com.treinamento.dojo.marvel.api.*;
import br.com.treinamento.dojo.marvel.config.MarvelApi;
import br.com.treinamento.dojo.marvel.model.Comic;
import br.com.treinamento.dojo.marvel.model.Character;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Responsável pelas chamadas da API de consulta.
 * Created by Alex Ricardo on 10/11/2016.
 */
@Component
public class CharacterService {

    @Autowired
    private MarvelApi marvelApi;
    @Autowired
    private LockMemoryCache lockMemoryCache;

    /**
     *
     * @param characterName
     * @return
     * @throws ServiceException
     */
    public ApiReturn<MarvelCharacter> getCharacter(String characterName) throws ServiceException {
        ApiReturn<MarvelCharacter> marvelCharacter = marvelApi.getCharacter(characterName);
        return marvelCharacter;
    }

    /**
     *
     * @param characterName
     * @return
     * @throws ServiceException
     */
    public Character getCharacterByName(String characterName) throws ServiceException {
        ApiReturn<MarvelCharacter> marvelCharacter = marvelApi.getCharacter(characterName);

        Character character = new Character();
        character.setName(marvelCharacter.getData().getResults().get(0).getName());
        character.setCharacterId(marvelCharacter.getData().getResults().get(0).getId());

        ApiReturn<Comic> comics = marvelApi.getComics(character.getCharacterId());
        character.setComics(comics.getData().getResults());

        LockMemoryCache.getInstance().add(character);

        return character;
    }

    /**
     *
     * @param characterId
     * @return
     * @throws ServiceException
     */
    public ApiReturn<Comic> getComicByID(Integer characterId) throws ServiceException{
        ApiReturn<Comic> marvelComic =  marvelApi.getComics(characterId);
        return marvelComic;
    }

}
