# README #

### Projeto Teste - Hands-On CI&T ###

* Cache
 - Idéia inicial era utilizar o EhCache, porém devido ao tempo hábil e problemas de infraestrutura não pude fazê-lo, optei então pela utilização de um Map.

* Rest Controller
 - Foram utilizados dois Rest Controllers, um para ações com a API Marvel através de personagens e outra para detail do que há em memória.

* API Local
 - Foram criadas classes de representatividade para exibição Json.

 - Foi criada uma classe MarvelApi para fazer a comunicação e autenticação com a API Marvel.

 - Criadas duas classes modelo para gerenciamento das informações e, tipagem de dados e objetos.

 - Criadas duas classes de serviço, uma que se comunica com a API e outra que faz a gerencia dos dados em "cache".

* Testes Unitários.
 - Foram gerados dois testes unitários, básicos e simples, um para a consulta via API e outro para a "cache".


* Necessário Java 8 e ApacheMaven para rodar a aplicação: mvn spring-boot:run

Obs. devido ao tempo disponível em horário comercial e limitação de estrutura para compilação, não pude explorar tecnologias mais complexas ou maneiras de implementar.
O projeto foi feito o mais simples possível.
